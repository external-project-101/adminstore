<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg-8">
        
        
        <?php foreach ($produk as $b)?>
        <?= $this->session->flashdata('message');?>
        <?php echo form_open_multipart('admin/update'); ?>

        <div class="form-group row">
                <label for="id_produk" class="col-sm-2 col-form-label">Produk Id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="id_produk" name="id_produk" value="<?= $b->id_produk ?>" readonly>
                </div>
            </div>
            
        <div class="form-group row">
                <label for="id_kategori" class="col-sm-2 col-form-label">Category Id</label>
                <div class="col-sm-10">
					<select class="form-control" id="id_kategori" name="id_kategori">
					<option value="">--Piih Kategori--</option>
					<?php foreach ($listkategori as $row ){
						$selected = ($row['id_kategori']==$b->id_kategori) ? 'selected=selected' : '';
						echo "<option value ='".$row['id_kategori']."' $selected >".$row['kategori']."</option>";
					} ?>
					</select>
                </div>
            </div>

            <div class="form-group row">
                <label for="judul" class="col-sm-2 col-form-label">Nama Product</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $b->nama ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="keterangan" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="keterangan" name="keterangan"><?= $b->keterangan ?></textarea>
                </div>
            </div>
			<div class="form-group row">
                <label for="harga" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="harga" name="harga" value="<?= $b->harga ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="stok" class="col-sm-2 col-form-label">Stock</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="stok" name="stok" value="<?= $b->stok ?>">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2">Image</div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <img id="img-file" src="<?= base_url('assets/img/uploads/') . $b->gambar ?>" class="img-thumbnail" class="gambar">
                            <input type="hidden" name="gambar_lama" value="<?= $b->gambar ?>">
                        </div>
                        <div class="col-sm-9">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="gambar" id="gambar">
                                <label class="custom-file-label">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-end">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                    <a href="<?= base_url('admin/produkdata'); ?>"><div class="btn  btn-danger">Cancel</div></a>

                </div>
            </div>
            
            <?php echo form_close(); ?>
         </div>
    </div>
</div>
    
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -- > 

