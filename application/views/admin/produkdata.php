<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
     <a href="<?= base_url('admin/produkReport') ?>" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm text-right"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
</div>
        <button class="btn btn-sm btn-primary mb-3 " data-toggle="modal" data-target="#addproduk"> Add Produk</button>
    
    <?php if (validation_errors()) : ?>
            <div class="alert alert-danger" role="alert">
                <?= validation_errors(); ?>
            </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
    <table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Produk Id</th>
        <th>Category</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Image</th>
        <th colspan="3">Action</th>
    </tr>
        <?php 
        $no = 1;
        foreach($produk as $b) : ?>
        <!-- buat ressult_array pakenya kaya yang dibawah  
        kalo result doang baru pake -> soalnya dia ngasilinya data object bukan data array
        -->
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $b["id_produk"] ?></td>
            <td><?= $b["kategori"] ?></td>
            <td><?= $b["nama"] ?></td>
            <td><?= $b["harga"] ?></td>
            <td><?= $b["stok"] ?></td>
            <td><?php if($b["gambar"]) { ?>
				<img src="<?php echo base_url('');?>assets/img/uploads/<?php echo $b["gambar"] ?> "width="70px" height="70px">
				<?php } else { echo "No Images"; } ?></td>
            <td>
                <?= anchor('admin/edit/' .$b["id_produk"], '<div class="btn btn-primary btn-sm mt-1"><i class="fa fa-edit"></i></div>') ?>
                
                <?= anchor('admin/delete/' .$b["id_produk"], '<div class="btn btn-danger btn-sm mt-1"><i class="fa fa-trash"></i></div>')?>
          </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="addproduk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addproduk">Add Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  
      <div class="modal-body">
      
      <?php echo form_open_multipart('admin/upload'); ?>
            <div class="form-group">
                <label>Category</label>
				<select class="form-control" id="id_kategori" name="id_kategori">
				<option value="">--Piih Kategori--</option>
				<?php foreach ($listkategori as $row ){
					echo "<option value ='".$row['id_kategori']."'>".$row['kategori']."</option>";
				} ?>
				</select>
                
            </div>
            
            <div class="form-group">
                <label>Name Product</label>
                <input type="text" name="nama" id="nama" class="form-control">
                
            </div>
            <div class="form-group">
                <label>Description</label>
				<textarea name='keterangan' id="keterangan" class="form-control"></textarea>                            
            </div>
            <div class="form-group">
                <label>Price</label>
                <input type="text" name="harga" id="harga" class="form-control">
            </div>
            <div class="form-group">
                <label>Stock</label>
                <input type="text" name="stok" id="stok" class="form-control">
            </div>
            <div class="form-group">
                <label>Image</label><br>
                <input type="file" name="gambar" id="gambar" class="form-control">
            </div>
            <div class="form-group">
                <img id="img-file" src="" class="img-thumbnail" class="gambar" width="100px">                
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>

      
      <?php echo form_close(); ?> 
   
        
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -- >     