<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Model_produk', 'produk');

    }

    public function index()
    {
        $data['title'] = 'All Products';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['produk'] = $this->produk->tampil_produk();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/index', $data);
        $this->load->view('templates/footer');
    }

    public function add_shopping($id_buku)
    {

        $produk = $this->produk->find($id_buku);
        $data = array(
            'id' => $produk->id_produk, 'qty' => 1, 'price' => $produk->harga, 'name' => $produk->nama
        );


        $this->cart->insert($data);
        redirect('dashboard');
    }

    public function detail_cart()
    {
        $data['title'] = 'Detail Cart';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/cart', $data);
        $this->load->view('templates/footer');

    }

    public function delete_cart()
    {
        $this->cart->destroy();
        redirect('dashboard/index');
    }

    public function checkout()
    {
        $data['title'] = 'Checkout';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/checkout', $data);
        $this->load->view('templates/footer');

    }

    public function proses()
    {
        $data['title'] = 'Proses';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $is_processed = $this->model_invoice->index();
        if ($is_processed) {
            $this->cart->destroy();
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('dashboard/proses', $data);
            $this->load->view('templates/footer');
        } else {
            echo "failed!";
        }
    }


    public function detail_produk($id_produk)
    {
        $data['title'] = 'Detail Buku';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['produk'] = $this->produk->detail_produk($id_produk);


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/detail_produk', $data);
        $this->load->view('templates/footer');

    }

    public function invoice()
    {
        $data['title'] = 'Invoice';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['invoice'] = $this->model_invoice->user_invoice($this->session->userdata('user_id'));


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/invoice', $data);
        $this->load->view('templates/footer');

    }

    public function detail($id_invoice)
    {
        $data['title'] = 'Detail Invoice';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['invoice'] = $this->model_invoice->ambil_id_invoice($id_invoice);
        $data['pesanan'] = $this->model_invoice->ambil_id_pesanan($id_invoice);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/detail_invoice', $data);
        $this->load->view('templates/footer');
    }

    public function bayar($id_invoice)
    {
        $data['title'] = 'Detail Pembayaran';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['admin'] = $this->db->get_where('user', ['id' => 6])->row_array();

        $data['invoice'] = $this->model_invoice->ambil_id_invoice($id_invoice);
        $data['pesanan'] = $this->model_invoice->ambil_id_pesanan($id_invoice);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('dashboard/bayar_invoice', $data);
        $this->load->view('templates/footer');
    }

    public function payment()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $is_processed = $this->model_invoice->payment();
		  if ($is_processed) {
			  redirect('dashboard/invoice');
		  } else {
			  echo "failed!";
		  }
    }
}